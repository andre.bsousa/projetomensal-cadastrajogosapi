const express = require('express')
const router = express.Router()
const User = require('../../models/user')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const config = require('config')
const { check, validationResult } = require('express-validator')


router.post('/', [
    check('email', 'Por favor insira um email valido').isEmail(),
    check('senha', 'Requer senha').exists()
], async (req, res) => {

    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }

    const { email, senha } = req.body

    try {

        let user = await User.findOne({ email })

        if (!user) {
            return res.status(404).json({ errors: [{ msg: 'Usuario não existe' }] })
        } else {

            const isMatch = await bcrypt.compare(senha, user.senha)

            if (!isMatch) {
                return res.status(404).json({ errors: [{ msg: 'Senha invalida' }] })
            } else {
                const payload = {
                    user: {
                        id: user.id,
                        nome: user.nome,
                        email: user.email
                    }
                }

                jwt.sign(payload, config.get('jwtSecret'), { expiresIn: '5 days' },
                    (err, token) => {
                        if (err) throw err
                        res.json({ token })
                    }
                )
            }

        }

    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }

})

module.exports = router