const express = require('express')
const lista_jogos = require('../../models/jogos')
const router = express.Router()

router.post('/', (req, res) => {
    const {id, nome, genero, distribuidora, desenvolvedora} = req.body

    /*if(req.header["code"] != 70){
        res.status(403).send({"error" : "forbidden"})
    }*/

    JSON.stringify(req.body)

    if(!nome){
        res.status(400).send({"error" : "requer nome"})
    } else {
        if(!genero){
            res.status(400).send({"error" : "requer genero do Jogo"})
        } else {

            if(!distribuidora){
                res.status(400).send({"error" : "requer distribuidora"})
            } else {

                if(!desenvolvedora){
                    res.status(400).send({"error" : "requer desenvolvedora"})
                } else{
                    lista_jogos.push({id, nome, genero, distribuidora, desenvolvedora})
                    console.log(`${id}, ${nome}, ${genero}, ${distribuidora}, ${desenvolvedora}`)
                    //res.send(req.body)
                    res.redirect(201, 'http://localhost:3000/jogos')

                }
            }

        }
        
    }


})

module.exports = router