const express = require('express')
const Jogos = require('../../models/jogos')
const router = express.Router()

router.get('/', async (req, res, next) => {
    try{
        const jogo = await Jogos.find({})
        res.json(jogo)
    } catch(err) {
        console.error(err.message)
        res.status(500).send('Server error')

    }
})

router.get('/:jogoId', async (req, res, next) => {
    try{
        const param_id = req.params["jogoId"]
        const jogo = await Jogos.findById({ _id: param_id })

        if(jogo){
            res.status(200).json(jogo)
        } else {
            res.status(400).send({"error" : "jogo não existe"})
        }
    } catch(err){
        console.error(err.message)
        res.status(500).send('Server error')

    }
})

router.post('/', [], async (req, res, next) => {
    
    try {

        let { nome, genero, distribuidora, desenvolvedora } = req.body
        let jogo = new Jogos({ nome, genero, distribuidora, desenvolvedora })
        await jogo.save()
        res.status(201).json(jogo)

    } catch (err){

    console.error(err.message)
    res.status(500).send({"error" : "Server Error"})
    }  
})

router.delete('/:jogoId', async (req, res, next) => {
    try{
        const param_id = req.params["jogoId"]
        const jogo = await Jogos.findByIdAndDelete({_id: param_id})
        if(jogo){
            res.status(200).json(jogo)
        } else {
            res.status(404).send({"error" : "Jogo não existe"})
        }

    } catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server error"})
    }
})

router.put('/:jogoId', async (req, res, next) => {
    try {
        const param_id = req.params["jogoId"]
        let { nome, genero, distribuidora, desenvolvedora } = req.body
        let update = { nome, genero, distribuidora, desenvolvedora }
        let jogo = await Jogos.findOneAndReplace({_id: param_id}, update, {new : true})
        if(jogo){
            res.status(200).json(jogo)
        } else {
            res.status(404).send({"error" : "Jogo não existe"})
        }

    } catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server error"})
    }
})

router.patch('/:jogoId', async (req, res, next) => {
    try {
        const param_id = req.params["jogoId"]
        let bodyRequest = req.body
        let update = { $set: bodyRequest }
        const jogo = await Jogos.findByIdAndUpdate({_id: param_id}, update, { new: true })
        if (jogo){

            
            res.status(200).json(jogo)

                }

                
            
        else {
            res.status(404).send({"error" : "Jogo não existe"})
        }

    } catch (err){
        console.error(err.message)
        res.status(500).send({"error" : "Server error"})

    }
})

module.exports = router