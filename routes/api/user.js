const express = require('express')
const Usuario = require('../../models/user')
const bcrypt = require('bcrypt')
const { check, validationResult } = require('express-validator')
const router = express.Router()
const auth = require('../../middleware/auth')

// @route   GET/user
// @desc    LIST user
// @acess   Private
router.get('/', auth,  async (req, res, next) => {
    try {
        const user = await Usuario.find({})
        res.json(user)
    } catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server error"})
    }
})

// @route   GET/user/:userId
// @desc    DETAIL user
// @acess   Public
router.get('/:userId', async (req, res, next) => {
    try{

        let params_id = req.params["userId"]
        const user = await Usuario.findById({_id: params_id})
        if(user){
            res.status(202).json(user)
        } else {
            res.status(404).send({"error" : "Usuario não existe"})
        }
    } catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server error"})
    }
})

// @route   POST/user
// @desc    CREATE user
// @acess   Public
router.post('/',[
    check('email', 'email invalido').isEmail(),
    check('nome', 'nome invalido').not().isEmpty(),
    check('senha', 'Por favor insira uma senha com 6 ou mais caracteres').isLength({min : 6})
], async (req, res, next) =>{
    try {
        let { nome, senha, email } = req.body
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({erros : errors.array() })
        } else {            

                const email_param = email
                const email_duplicado = await Usuario.findOne({email: email_param}, email)
                //if(email != email_duplicado){

                if(!email_duplicado){
                    let usuario = new Usuario({ nome, senha, email })
                    const salt = await bcrypt.genSalt(10)
                    usuario.senha = await bcrypt.hash(senha, salt)        
                    await usuario.save()      
                    res.status(201).json(usuario)                                

                } else {
                    res.status(500).json({"error" : "Email duplicado"})
                }

                


        //}
    }
    } catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server error"})
    }
})

// @route   DELETE/user
// @desc    DELETE user
// @acess   Public
router.delete('/:email', async (req, res, next) => {
    try {
        let param_email = req.params["email"]
        const user = await Usuario.findOneAndDelete({ email: param_email })
        if (user){
            res.status(204).json(user)
        } else {
            res.status(404).send({"error" : "User does not exist"})
        }

    } catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server error"})
    }
})

// @route    PUT /user/:userId
// @desc     EDIT user
// @access   Public
router.put('/:email', [
    check('email', 'Email invalido').isEmail(),
    check('nome', 'Nome invalido').not().isEmpty(),
    check('senha', 'Por favor insira uma senha com 6 ou mais caracteres').isLength({min : 6})
], async (req, res, next) => {
    try{
        const errors = validationResult(req)
        if(!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() })
        }

        let param_email = req.params["email"]
        let { nome, email, senha } = req.body
        let update = { nome, email, senha }
        const salt = await bcrypt.genSalt(10);
        update.senha = await bcrypt.hash(senha, salt);        
        let user = await Usuario.findOneAndReplace({email : param_email}, update, {new: true})
        if(user){
          res.status(200).json(user)

        } else {
            res.status(404).send({"error" : "Usuario não existe"})
        }

    } catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server error"})
    }

})

// @route   PATCH/user/:userId
// @desc    PARTIAL EDIT user
// @acess   Public
router.patch('/:userId', async (req, res, next) =>{
    try{
        /*const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({errors : errors.array() })
        }*/

        const id = req.params.userId
        const salt = await bcrypt.salt(10)

        let bodyRequest = req.body

        if(bodyRequest.senha){
            bodyRequest.senha = await bcrypt.hash(bodyRequest.senha, salt)
        }

        const update = { $set: bodyRequest }
        const user = await Usuario.findByIdAndUpdate(id, update, {new: true})

        if(user){
            res.status(200).json(user)
        } else {
            res.status(404).send({"error": "Usuario não existe"})
        }


    } catch(err){
        console.error(err.message)
        res.send(500).send({"error" : "Server error"})
    }
})
module.exports = router