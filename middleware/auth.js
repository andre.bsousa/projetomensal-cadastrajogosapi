const jwt = require('jsonwebtoken')
const config = require('config')


module.exports = function (req, res, next) {

    const token = req.header('x-auth-token')

    if(!token){
        return res.status(401).json({ msg: 'Sem token, autorização negada' })
    }

    try {

        jwt.verify(token, config.get('jwtSecret'), (error, decoded) => {
            if(error){
                return res.status(401).json({ msg: 'Token invalido' })
            } else {
                req.user = decoded.user
                next()
            }
        })

    } catch(err){
        console.error('Algo deu errado com o middlware')
        res.status(500).json({ msg: 'Server error' })
    }
}