const mongoose = require('mongoose')

const usuarioSchema = new mongoose.Schema ({
    nome: { 
        type: String,
        required: true
    },

    senha: { 
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true,
        unique: true
    } 

}, { autoCreate: true })

/*let lista_usuarios = []

class Usuario {
    constructor (id, nome, senha, email){
        this.id = this.gerarId()
        this.nome = this.nome
        this.senha = this.senha
        this.email = this.email
    }

    gerarId(){
        if(lista_usuarios.length === 0){
            return 1
        }
        return lista_usuarios[lista_usuarios.length -1].id +1
    }
}

const salt = bcrypt.genSaltSync(10)

lista_usuarios.push(new Usuario("Ricky", bcrypt.hashSync("12345", salt), "ricky@teste.com"))
lista_usuarios.push(new Usuario("Morty", bcrypt.hashSync("12223242", salt), "morty@teste.com"))

module.exports = {
    Usuario,
    lista_usuarios
}*/

module.exports = mongoose.model('Usuario', usuarioSchema)