const express = require('express')
const app = express()
var cors = require('cors')
const connectDB = require('./config/db')
const PORT = process.env.PORT || 8080
var bodyparser = require('body-parser')

app.use(cors())
app.use(express.json())
app.use(bodyparser.urlencoded({extended:true}))
app.use(bodyparser.json())

connectDB()

/*app.use ( (req, res, next) => {
    if (req.secure) {
            // request was via https, so do no special handling
            next();
    } else {
            // request was via http, so redirect to https
            res.redirect('https://' + req.headers.host + req.url);
    }
});*/


app.use('/', require('./routes/ola'))
app.use('/envio', require('./routes/api/envio'))
app.use('/jogos', require('./routes/api/jogos'))
app.use('/user', require('./routes/api/user'))
app.use('/auth', require('./routes/api/auth'))

app.listen(PORT, () => console.log(`Rodando em htpp://localhost:${PORT}`))
